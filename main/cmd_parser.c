/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * Copyright Benjamin Aigner, 2019 <beni@asterics-foundation.org>
 * 
 * This program is an example implementation for a command parser,
 * which tries to use static values as much as possible to reduce
 * heap/stack usage.
 */

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include <esp_log.h>
//we do need a lot of strncmp, strncat, strncpy, strnlen,...
#include <string.h>


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Example project (might be useless for your project)
 * -) UART settings
 * -) Testing data (+expected output)
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


#define ECHO_TEST_TXD  (GPIO_NUM_4)
#define ECHO_TEST_RXD  (GPIO_NUM_5)
#define ECHO_TEST_RTS  (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS  (UART_PIN_NO_CHANGE)

/// Buffer size for receiving UART data (before parsing)
#define BUF_SIZE (1024)

/** These are test commands, executing handlers (or failing) */
#if 0
char* testcmds[] = {
    //should work
  "AT NN\r",
  "AT IN 10\r",
  "AT IN -10\r",
    //should fail
    "AT IN -11\r",
    "AT IN 11\r",
    "AT WS -300000\r",
    "AT LU 30000000000\r",
    "AT E1 30\r",
    ""
};
//we don't check for expected array status
struct targetS output[] = NULL;
#endif
/** These are test commands, modifying the struct */
#if 1
//these are the executed commands
//note: must be terminated with an empty string
char * testcmds[] = {
  "AT BU 10\r",
  "AT BU 0\r",
  "AT BU 255\r",
  "AT BS -127\r",
  "AT BS 0\r",
  "AT BS 127\r",
  "AT WU 4000\r",
  "AT WU 3000\r",
  "AT WU 0\r",
  "AT WS -3000\r",
  "AT WS -300\r",
  "AT WS 0\r",
  "AT LS -300000\r",
  "AT LU 300000\r",
  "AT LU 0\r",
  "AT LS 0\r",
  "AT BS 0\r",
  "AT BU 0\r",
  ""
};
//this is the data which should be in the target struct
//after executing the corresponding line from the testcmds
//note: must have at least the same amount of values as testcmds!
struct targetS output[] = {
    {10,0,0,0,0,0},
    {0,0,0,0,0,0},
    {255,0,0,0,0,0},
    {255,-127,0,0,0,0},
    {255,0,0,0,0,0},
    {255,127,0,0,0,0},
    {255,127,4000,0,0,0},
    {255,127,3000,0,0,0},
    {255,127,0,0,0,0},
    {255,127,0,-3000,0,0},
    {255,127,0,-300,0,0},
    {255,127,0,0,0,0},
    {255,127,0,0,0,-300000},
    {255,127,0,0,300000,-300000},
    {255,127,0,0,0,-300000},
    {255,127,0,0,0,0},
    {255,0,0,0,0,0},
    {0,0,0,0,0,0},
};
#endif
#define TESTING


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Static parser settings.
 * Needs to be set accordingly to your project/command set
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

///Prefix, which is located directly prior to any implemented command
#define CMD_PREFIX      "AT "
///Maximum length of a command (EXcluding the prefix, e.g. "AT RO" with prefix "AT " results in length 2)
#define CMD_LENGTH      2
///Maximum length of a command (including parameters, prefix and command itself -> full line)
#define CMD_MAXLENGTH   50
/** @brief Declare array target type
 * The parser config includes an "offsetof" call. To have a constant offset
 * value, this needs to be known before we build the command array */
#define CMD_TARGET_TYPE struct targetS



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * From here, we have internal parts of the parser.
 * If you want to use this parser in your project, move these
 * lines to an external file (or just C&P :-))
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/** @brief Handler function pointer for a recognized command
 * @note First parameter is the full received string
 * @note Although we have void* parameters,
 * the given data is either an (int32_t) or a (char*), depending
 * on given parameter types.*/
typedef esp_err_t(*cmd_handler)(char* , void* , void* );



/** @brief Type of parameter for a command */
typedef enum ParamType {
    PARAM_NONE,
    /** Parameter field is unused. 
     * @warning Do not used as first param type, if the second one is used! */
    PARAM_NUMBER,
    /** Parameter field is interpreted as integer number
     * @note The value will be parsed to an int32_t*/
    PARAM_STRING
    /** Parameter field is copied and passed as a string
     * @note We malloc in the parser for each string parameter. */
}cmd_paramtype;



/** @brief Return status of command parser */
typedef enum ParserRetVal {
    PREFIXONLY,
    /** Special case: only the prefix is detected */
    SUCCESS,
    /** Everything went fine, command+params are valid & handler is executed */
    NOCOMMAND,
    /** String itself was OK (long enough, terminated,...), but no associated
     * command was found */
    HANDLERERROR,
    /** Either the handler returned an error (!= ESP_OK) or the handler
     * itself was not set */
    FORMATERROR,
    /** Formatting error, possible reasons:
     * * too short
     * * prefix missing
     */
    PARAMERROR,
    /** Parameter errors, possible reasons:
     * * parameters out of range
     * * missing parameters
     * * missing space between two parameters
     * * number<->string position changed
     */
     POINTERERROR,
     /** If you want to modify an array with this command (if handler is NULL),
      * this return value signals an invalid pointer */
} cmd_retval;

/** @brief If a field should be modified, this will be the target type
 * @note Only basic datatypes are used! If you want to modify an enum or
 * complex structure, use a handler and modify data there. */
typedef enum ParserTargetType {
  NOCAST,
  /** Do NOT do anything with the pointer + offset +*/
  UINT8, 
  /** Parse data to target uint8_t */
  INT8,
  /** Parse data to target int8_t */
  UINT16, 
  /** Parse data to target uint16_t */
  INT16,
  /** Parse data to target int16_t */
  UINT32, 
  /** Parse data to target uint32_t */
  INT32,
  /** Parse data to target int32_t */
} cmd_typecast;

/** @brief Type for one new command
 * 
 * @note Either you set a handler fct pointer (so the handler field
 * is != NULL) and this handler will be executed on this command. Or: if
 * this field is left null, the general cfg will be changed, according
 * to type (target data type) and offset within the array
 * @note If the data modification is used, only one numerical parameter
 * will be parsed! */
typedef struct OneCmd{
    /** String which will be recognized as this command.
     * @see CMD_LENGTHv */
    char name[CMD_LENGTH+1];  //command name. e.g. "RO", combined with "AT " as prefix -> command "AT RO"; including \0 here (-> +1)
    /** Array of parameter types. Currently maximum 2 parameters are implemented. */
    cmd_paramtype ptype[2];
    /** Array of minimal values (either min value of an integer or min length of a string) */
    int64_t min[2];        //minimal value for param1 and param2
    /** Array of maximum values (either max value of an integer or max length of a string) */
    int64_t max[2];        //maximum value for param1 and param2
    /** Hander which will be executed if this command is recognized and the parameters are valid */
    cmd_handler handler;     //action1, triggered if command is recognized
    /** 2nd possible action: modify an offset (in our case: change currentCfg) */
    size_t offset;
    /** ad 2nd To know the casting, we need to specify a type of the target here */
    cmd_typecast type;
} cmd;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Example command handlers.
 * Mostly useless for your use-case. Please change
 * accordingly
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

///@brief Handler which don't parses any parameters
esp_err_t cmdNoParam(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed %s",at);
    return ESP_OK;
}
///@brief Handler which parses one int32 param
esp_err_t cmdParamInt(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s|;; param: |%d|",at,(int32_t)param);
    return ESP_OK;
}
///@brief Handler which parses two int32 param2
esp_err_t cmdParam2Int(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s| ;; param: |%d| param2: |%d|",at,(int32_t)param,(int32_t)param2);
    return ESP_OK;
}
///@brief Handler which parses one string and one int32 param
esp_err_t cmdStringAndInt(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s| ;; param: |%s| param2: |%d|",at,(char*)param,(int32_t)param2);
    return ESP_OK;
}
///@brief Handler which parses one int32 param and one string
esp_err_t cmdIntAndString(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s| ;; param: |%d| param2: |%s|",at,(int32_t)param,(char*)param2);
    return ESP_OK;
}
///@brief Handler which parses one string param
esp_err_t cmdParamString(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s| ;; param: |%s|",at,(char*)param);
    return ESP_OK;
}
///@brief Handler which parses two string params
esp_err_t cmdParam2String(char* at, void *param, void *param2)
{
    ESP_LOGW("par","Parsed |%s| ;; param: |%s| param2: |%s|",at,(char*)param,(char*)param2);
    return ESP_OK;
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Example target struct.
 * Also useless for your project. Use some kind of config
 * struct you want to modify directly with commands.
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/** @brief target data structure. this might be some config/saved data/...
 * @note This data will be modified by the parser. If you want to avoid
 * any data races (if you use more tasks accessing it), use a mutex!
 */
struct targetS {
    uint8_t firstbyte;
    int8_t secondbyte;
    uint16_t firstword;
    int16_t secondword;
    uint32_t firstint;
    int32_t secondint;
};
struct targetS *ourdata;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * MAIN COMMAND TABLE
 * This command table is used to determine valid commands
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/** @brief Constant command table
 * 
 * This array of cmd structs implements one command for each element
 * of this array. It is initialized constant, therefore we don't need
 * RAM on runtime (nothing is allocated, nothing is loaded to stack).
 * 
 * @note Currently, there are a few test commands implemented.
 * First character represents the first parameter, second one the second.
 * parameter. 'N': unused parameter, 'S': string parameter, 'I': integer parameter
 * @see cmd
 */
const cmd commands[] = {
    //"AT NN" no parameters
  {"NN", {PARAM_NONE,PARAM_NONE},{0,0},{0,0},cmdNoParam,0,NOCAST},
  
    //"AT IN" one number as parameter, ranging from -10 to +10
  {"IN", {PARAM_NUMBER,PARAM_NONE},{-10,0},{10,0},cmdParamInt,0,NOCAST},

    //"AT SN" one string (3-8 characters)
  {"SN", {PARAM_STRING,PARAM_NONE},{3,0},{8,0},cmdParamString,0,NOCAST},

    //"AT SI" one string first (3-8 characters), one number after, ranging from -127 to +127
    //NOTE: the number will be parsed from the LAST space character onwards
  {"SI", {PARAM_STRING,PARAM_NUMBER},{3,-127},{8,127},cmdStringAndInt,0,NOCAST},

    //"AT IS" on number first, ranging from -20 to +20, one string after (3-8 characters)
    //NOTE: the string will be parsed from the first space character after the number
  {"IS", {PARAM_NUMBER,PARAM_STRING},{-20,3},{20,8},cmdIntAndString,0,NOCAST},

    //"AT II" two numbers, first ranging from 0-500, second from 511 to 1023
  {"II", {PARAM_NUMBER,PARAM_NUMBER},{0,511},{500,1023},cmdParam2Int,0,NOCAST},

    //"AT SS" two strings, both 3-20 chars long
    //NOTE: the second string starts with the last space character!
    //e.g.: AT SS bender's shiny metal a** would result in:
    //"bender's shiny metal"; "a**"
  {"SS", {PARAM_STRING,PARAM_STRING},{3,3},{20,20},cmdParam2String,0,NOCAST},
  
    //"AT BU" modify one unsigned byte.
  {"BU", {PARAM_NUMBER,PARAM_NONE},{0,0},{255,0},NULL,offsetof(struct targetS,firstbyte),UINT8},
    //"AT BS" modify one signed byte.
  {"BS", {PARAM_NUMBER,PARAM_NONE},{-127,0},{127,0},NULL,offsetof(struct targetS,secondbyte),INT8},
  
    //"AT WU" modify one unsigned word
  {"WU", {PARAM_NUMBER,PARAM_NONE},{0,0},{65535,0},NULL,offsetof(struct targetS,firstword),UINT16},
    //"AT WS" modify one signed word
  {"WS", {PARAM_NUMBER,PARAM_NONE},{-32768,0},{32767,0},NULL,offsetof(struct targetS,secondword),INT16},
  
    //"AT LU" modify one unsigned integer. I had to look up this number :-) But useful, because we maybe need to type it in somewhere :-)
  {"LU", {PARAM_NUMBER,PARAM_NONE},{0,0},{4294967295,0},NULL,offsetof(struct targetS,firstint),UINT32},
    //"AT LS" modify one signed integer 
  {"LS", {PARAM_NUMBER,PARAM_NONE},{-2147483647,0},{2147483647,0},NULL,offsetof(struct targetS,secondint),INT32},
    //"AT E1" will generate a POINTERERROR, writing out of bounds
  {"E1", {PARAM_NUMBER,PARAM_NONE},{-2147483647,0},{2147483647,0},NULL,offsetof(struct targetS,secondint)+4,INT8}
};



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * MAIN PARSER
 * This must be copied to your project, hopefully in an
 * external file with a valid GPL notice :-)
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/** @brief Main parser
 * 
 * This parser is called with one finished (and 0-terminated line).
 * It does following steps:
 * * Parameter checking (empty target pointer, empty data string)
 * * Input length checking (should be at least the prefix)
 * * If the prefix is the only data, it will return a special case
 * * Searching through the constant command array
 * * If a match is found, the command parameter structure is checked
 * * Validating input values against the given ranges
 * * Executing the handler (if it is != NULL) or modifying the target struct
 * 
 * 
 * @return See cmd_retval. SUCCESS on success.
 */
cmd_retval cmdParser(char * data, struct targetS *target)
{
    uint32_t len; //length of input string, 
    esp_err_t retval = ESP_FAIL; //return value of handler
    uint16_t i; //general index
    uint16_t matchedcmds = 0; //count of matched&executed commands
    
    //1.) check for valid pointers
    if(data == NULL) return ESP_FAIL;
    if(target == NULL) return POINTERERROR;
    
    //2.) check if this string is terminated
    //iterate over the string, as long as we don't reach max size or
    //find a \0
    for(i = 0; i<CMD_MAXLENGTH; i++) if(data[i] == '\0') break;
    //if this iteration went until the end, this string is unterminated
    if(i == CMD_MAXLENGTH) return FORMATERROR;
    
    //3.) check for length, should be at least the prefix
    len=strnlen(data,CMD_MAXLENGTH);
    if(len==strlen(CMD_PREFIX) || len==(strlen(CMD_PREFIX)-1))
    {
        //note we check the prefix minus 1 character.
        //helps us if we have "AT" or "AT "
        if(strncasecmp(data,CMD_PREFIX,strlen(CMD_PREFIX)-1) == 0) return PREFIXONLY;
        else return FORMATERROR;
    }
    if(len < (strlen(CMD_PREFIX) + CMD_LENGTH))
    {
        //command is shorter than prefix + command
        //(e.g. "AT RO" -> "AT " 3 + "RO" 2)
        return FORMATERROR;
    }
    //4.) check for the prefix
    if(strncasecmp(data,CMD_PREFIX,strlen(CMD_PREFIX)) != 0)
    {
        //didn't got the prefix, return
        return FORMATERROR;
    }
    
    //5.) now search array of commands for a match
    //
    ESP_LOGV("parser","Comparing: %s with:::",&data[strlen(CMD_PREFIX)]);
    for(uint32_t id = 0; id<(sizeof(commands) / sizeof(cmd)); id++)
    {
        //we iterate over the whole array. length is determined by
        //getting size of full command array and dividing it by size of one command
        
        //compare the command strings
        ESP_LOGV("parser","%s, result: %d", commands[id].name, \
            strncasecmp(&data[strlen(CMD_PREFIX)],commands[id].name,CMD_LENGTH));
        if(strncasecmp(&data[strlen(CMD_PREFIX)],commands[id].name,CMD_LENGTH) == 0)
        {
            //found that command.
            //now we want to execute this command:
            //a.) extract parameters accordingly
            //b.) we need to check the parameter(s) for validity
            //c.) execute handler / modify target data
            //d.) cleanup
            
            //possible future parameters for handlers
            //int32_t parami[2] = {0,0};
            //char* params[2] = {NULL,NULL};
            void * paramFinal[2] = {NULL,NULL};
            uint16_t offsetStart = 0;
            uint16_t offsetEnd = 0;
            char * e;
            
            ESP_LOGD("parser","Found matching cmd at %d",id);
            
            //a.) do the parameter parsing/extraction for both possible active parameters.
            for(i = 0; i<2; i++)
            {
                switch(commands[id].ptype[i])
                {
                    case PARAM_NONE: break; //nothing to do here.
                    case PARAM_NUMBER:
                        //set offset accordingly:
                        //first parameter starts after prefix, command and a space
                        if(i == 0) offsetStart = strlen(CMD_PREFIX) + CMD_LENGTH + 1;
                        //second parameter: we start search first space from the end.
                        if(i == 1)
                        {
                            char* t = &data[len];
                            while(t-- != data && *t != ' ');
                            offsetStart = t-data;
                        }
                    
                        //parse the parameter for a number
                        paramFinal[i] = (void*)strtol(&data[offsetStart],&e,10);
                        //with endptr, we can check if there was a number at all
                        if(e==&data[offsetStart]) return PARAMERROR;
                        
                        ESP_LOGD("parser","Param %d, int: %d",i,(int32_t)paramFinal[i]);
                        //b.) parameter check
                        if((int32_t)paramFinal[i] > commands[id].max[i] || (int32_t)paramFinal[i] < commands[id].min[i]) return PARAMERROR;
                        break;
                    case PARAM_STRING:
                        //for strings we always need the last appearing space char
                        e = &data[len];
                        while(e-- != data && *e != ' ');
                        ESP_LOGV("parser","last space @%d",e-data);
                    
                        //set offset accordingly:
                        //first parameter starts after prefix, command and a space
                        if(i == 0)
                        {
                            offsetStart = strlen(CMD_PREFIX) + CMD_LENGTH + 1;
                            //end is either determined by string length
                            //if there is no space except at the beginning
                            if(e-data <= CMD_LENGTH + strlen(CMD_PREFIX)) offsetEnd = len;
                            //or it is defined by the last occuring space char
                            else offsetEnd = e-data;
                            //special case: one parameter string: don't split at spaces.
                            if(commands[id].ptype[i+1] == PARAM_NONE) offsetEnd = len;
                        }
                        //second parameter: we start from character after last space
                        if(i == 1)
                        {
                            offsetStart = e-data+1;
                            //special case: the detected space character is equal to
                            //the space between cmd and parameter ->
                            //we don't want to include the previous int
                            //-> abort
                            if(offsetStart == strlen(CMD_PREFIX)+CMD_LENGTH+1) return PARAMERROR;
                            //another special case: first a int, then a string.
                            //don't split the string at spaces, especially not from back to front
                            if(commands[id].ptype[i-1] == PARAM_NUMBER)
                            {
                                offsetStart = strlen(CMD_PREFIX)+CMD_LENGTH+1;
                                while(data[offsetStart] != '\0' && data[offsetStart++] != ' ');
                            }
                            offsetEnd = len;
                        }
                        ESP_LOGV("parser","str from %d to %d",offsetStart,offsetEnd);
                        
                        //b.) param check
                        if((offsetEnd - offsetStart) > commands[id].max[i] || \
                            (offsetEnd - offsetStart) < commands[id].min[i]) return PARAMERROR;
                        
                        //allocate memory for this string
                        paramFinal[i] = malloc(offsetEnd - offsetStart+1);
                        if(paramFinal[i] == NULL) return FORMATERROR;
                        //save the string pointer.
                        //Note: no \0  included here.
                        strncpy(paramFinal[i],&data[offsetStart],offsetEnd-offsetStart);
                        //terminate the new string
                        ((char*)paramFinal[i])[offsetEnd-offsetStart] = '\0';
                        ESP_LOGD("parser","Param %d, str: %s",i,(char*)paramFinal[i]);
                        break;
                }
            }
            
            //c.) Now we either execute the handler or modify data
            if(commands[id].handler == NULL)
            {
                //cast the parsed data into the given target type
                //note: we check for each size individually if we
                //write only within target
                size_t length = 0;
                
                switch(commands[id].type)
                {
                    case UINT8: 
                    case INT8: 
                        length = 1;
                        retval = ESP_OK;
                        break;
                    case UINT16: 
                    case INT16: 
                        length = 2;
                        retval = ESP_OK;
                        break;
                    case UINT32: 
                    case INT32: 
                        length = 4;
                        retval = ESP_OK;
                        break;
                    case NOCAST: break; //should not be here. No handler and no given casting...
                }
                //if we found a match, try to copy
                if(retval == ESP_OK)
                {
                    //check for limits
                    if(commands[id].offset > sizeof(CMD_TARGET_TYPE)-length) retval = ESP_FAIL;
                    else memcpy(&(((uint8_t *)target)[commands[id].offset]),&paramFinal[0],length);
                }
            } else retval = commands[id].handler(data,paramFinal[0],paramFinal[1]);
            
            //d.) cleanup (free allocated strings)
            if(commands[id].ptype[0] == PARAM_STRING && paramFinal[0] != NULL) free(paramFinal[0]);
            if(commands[id].ptype[1] == PARAM_STRING && paramFinal[1] != NULL) free(paramFinal[1]);
            matchedcmds++;
            
            //stop if the handler was not successful
            if((commands[id].handler != NULL) && (retval != ESP_OK)) return HANDLERERROR;
            //or if had some kind of pointer error
            if((commands[id].handler == NULL) && (retval != ESP_OK)) return POINTERERROR;
        }
        //if no match, continue search
    }
    ESP_LOGD("parser","Searched %d commands, found %d",(sizeof(commands) / sizeof(cmd)),matchedcmds);
    if(matchedcmds != 0) return SUCCESS;
    else return NOCOMMAND;
}


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Main task, either using UART input or teststrings
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/** Main task */
static void echo_task()
{

    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, ECHO_TEST_TXD, ECHO_TEST_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS);
    uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);

    // Configure a temporary buffer for the incoming data
    uint8_t *data = (uint8_t *) malloc(BUF_SIZE);
    ourdata = (struct targetS *)malloc(sizeof(struct targetS));
    memset(ourdata,0,sizeof(struct targetS));
    uint16_t offset = 0;
    uint16_t testnr = 0;
    
    while (1) {
        //use this block if you want to test this with real UART data
        #if 0
            // Read data from the UART (concat to one line with offset)
            int len = uart_read_bytes(UART_NUM_1, data+offset, BUF_SIZE-offset, 20 / portTICK_RATE_MS);
            //if we read something, set the offset to the end of
            //the currently received data, upcoming data will be appended
            if(len != -1) offset+=len;
        #endif
        //or use this block for testdata
        #ifdef TESTING
            if(testcmds[testnr] != NULL)
            {
                strncpy((char*)data,testcmds[testnr],BUF_SIZE);
                offset = strnlen((char*)data,BUF_SIZE);
                ESP_LOGW("testing","Struct before call:");
                ESP_LOG_BUFFER_HEXDUMP("testing",ourdata,sizeof(struct targetS),ESP_LOG_WARN);
                ESP_LOGW("testing","values: %u, %d, %u, %d",ourdata->firstbyte,ourdata->secondbyte,ourdata->firstword,ourdata->secondword);
                ESP_LOGW("testing","Upcoming command: %s",data);
                ESP_LOGW("testing","Sizeof: %d, %d, %d, %d, %d, %d",sizeof(uint8_t),sizeof(int8_t),sizeof(uint16_t),sizeof(int16_t),sizeof(uint32_t),sizeof(int32_t));
            }
        #endif
        
        /** This is the parser call, which can be implemented in your project */
        //check if we got a line termination either by \r or \n
        for(uint16_t i = 0; i<offset;i++)
        {
            if(data[i] == '\r' || data[i] == '\n')
            {
                //print stack/heap usage
                ESP_LOGD("main","Free heap before parsing: %d",xPortGetFreeHeapSize());
                ESP_LOGD("main","Stack high water mark: %d",uxTaskGetStackHighWaterMark(NULL));
        
                //terminate this string
                //data[i+1] = 0; //with this command, you will have either \r or \n at the end
                data[i] = 0; //with this command trailing \r or \n are removed
                //if yes, start the parser
                switch(cmdParser((char*)data,ourdata))
                {
                    case FORMATERROR: ESP_LOGE("main","FORMATERROR"); break;
                    case HANDLERERROR: ESP_LOGE("main","HANDLERERROR"); break;
                    case NOCOMMAND: ESP_LOGE("main","NOCOMMAND"); break;
                    case PARAMERROR: ESP_LOGE("main","PARAMERROR"); break;
                    case PREFIXONLY: ESP_LOGE("main","PREFIXONLY"); break;
                    case SUCCESS: ESP_LOGI("main","SUCCESS"); break;
                    case POINTERERROR: ESP_LOGE("main","POINTERERROR"); break;
                }
                //and reset the offset to 0 (overwrite the old command)
                offset = 0;
                //notification
                ESP_LOGI("main","rec: %s",data);
                break;
            }
        }
        
        //validate after testing
        #ifdef TESTING
            if(output != NULL)
            {
                ESP_LOGW("testing","Comparing struct");
                if(memcmp(ourdata,(void*)&output[testnr],sizeof(struct targetS)) != 0)
                {
                    ESP_LOGE("testing","Failed, struct mismatch!");
                    vTaskDelay(200/portTICK_PERIOD_MS);
                    abort();
                }
            }
            //next command
            testnr++;
            if(testcmds[testnr][0] == 0) testnr = 0;
            vTaskDelay(2000/portTICK_PERIOD_MS); //delllllaaaayyyy
        #endif
        
    }
}

void app_main()
{
    xTaskCreate(echo_task, "uart_echo_task", 4096, NULL, 10, NULL);
}
