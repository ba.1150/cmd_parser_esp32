/*
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * TEST FILE!!!!!!!!!!!!!
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
*/

#ifndef FUNCTION_COMMON_H_
#define FUNCTION_COMMON_H_

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/portmacro.h>
#include "freertos/task.h"
#include "freertos/queue.h"
#include <esp_log.h>

#include "driver/rmt.h"

  #define VB_INTERNAL2    0
  #define VB_INTERNAL1    1
  #define VB_EXTERNAL1    2
  #define VB_EXTERNAL2    3
  #define VB_UP           4
  #define VB_DOWN         5
  #define VB_LEFT         6
  #define VB_RIGHT        7
  #define VB_SIP          8
  #define VB_STRONGSIP    9
  #define VB_PUFF         10
  #define VB_STRONGPUFF   11
  #define VB_STRONGSIP_UP     12
  #define VB_STRONGSIP_DOWN   13
  #define VB_STRONGSIP_LEFT   14
  #define VB_STRONGSIP_RIGHT  15
  #define VB_STRONGPUFF_UP    16
  #define VB_STRONGPUFF_DOWN  17
  #define VB_STRONGPUFF_LEFT  18
  #define VB_STRONGPUFF_RIGHT 19
  #define VB_MAX          20


/** special virtual button, which is used to trigger a task immediately.
 * After this single action, each function body of functional tasks
 * is REQUIRED to do a return. */
#define VB_SINGLESHOT   32


/*++++ MAIN CONFIG STRUCT ++++*/

/**
 * Mode of operation for the mouthpiece:<br>
 * NONE       Do not do anything with the mouthpiece
 * MOUSE      Mouthpiece controls mouse cursor <br>
 *            Used parameters: <br>
 *              * max_speed
 *              * acceleration
 *              * deadzone_x/y
 *              * sensitivity_x/y
 * JOYSTICK   Mouthpiece controls two joystick axis <br>
 *            Used parameters <br>
 *              * deadzone_x/y
 *              * sensitivity_x/y
 *              * axis
 * THRESHOLD  Mouthpiece triggers virtual buttons <br>
 *            Used parameters <br>
 *              * deadzone_x/y
 *              * threshold_x/y
 * @see VB_UP
 * @see VB_DOWN
 * @see VB_LEFT
 * @see VB_RIGHT
 * */
typedef enum mouthpiece_mode {NONE, MOUSE, JOYSTICK, THRESHOLD} mouthpiece_mode_t;

/**
 * config for the ADC task & the analog mode of operation
 * 
 * TBD: describe all parameters...
 * 
 * @todo Remove report raw from here & create new "volatile" config struct -> no need to save, but no need to overwrite on slot change
 * @see mouthpiece_mode_t
 * @see VB_SIP
 * @see VB_PUFF
 * @see VB_STRONGSIP
 * @see VB_STRONGPUFF
 * */
typedef struct adc_config {
  /** mode setting for mouthpiece, @see mouthpiece_mode_t */
  mouthpiece_mode_t mode;
  /** acceleration for x & y axis (mouse & joystick mode) */
  uint8_t acceleration;
  /** max speed for x & y axis (mouse & joystick mode) */
  uint8_t max_speed;
  /** deadzone values for x & y axis (mouse & joystick & threshold mode) */
  uint8_t deadzone_x;
  uint8_t deadzone_y;
  /** sensitivity values for x & y axis (mouse & joystick mode) */
  uint8_t sensitivity_x;
  uint8_t sensitivity_y;
  /** pressure sensor, sip threshold */
  uint16_t threshold_sip;
  /** pressure sensor, puff threshold */
  uint16_t threshold_puff;
  /** pressure sensor, strongsip threshold */
  uint16_t threshold_strongsip;
  /** pressure sensor, strongpuff threshold */
  uint16_t threshold_strongpuff;
  /** Enable report RAW values (!=0), values are sent via halSerialSendUSBSerial */
  uint8_t reportraw;
  /** joystick axis assignment TBD: assign axis to numbers*/
  uint8_t axis;
  /** FLipMouse orientation, 0,90,180 or 270° */
  uint16_t orientation;
  /** On-the-fly calibration, count of idle events before triggering calibration */
  uint8_t otf_count;
  /** On-the-fly calibration, level of detecting idle (all raw values need to change less
   * than this value to be detected as idle) */
  uint8_t otf_idle;
} adc_config_t;

/** @brief Type of VB command
 * @see vb_cmd_t */
typedef enum {
  T_CONFIGCHANGE, /** @brief Config change request */
  T_CALIBRATE, /** @brief Calibrationrequest */
  T_SENDIR, /** @brief Send an IR command */
  T_MACRO /** @brief Trigger macro execution */
} vb_cmd_type_t;

typedef struct generalConfig {
  uint32_t slotversion;
  adc_config_t adc;
  uint8_t ble_active;
  uint8_t usb_active;
  /** mouse wheel: stepsize */
  uint8_t wheel_stepsize;
  /** country code to be used by BLE&USB HID */
  uint8_t countryCode;
  /** keyboard locale to be used by BLE&USB(serial) HID */
  uint8_t locale;
  /** device identifier to be used by BLE&USB HID.
   * 0 => FLipMouse
   * 1 => FABI */
  uint8_t deviceIdentifier;
  /** @brief Timeout between IR edges before command is declared as finished */
  uint8_t irtimeout;
  /** @brief Global anti-tremor time for press */
  uint16_t debounce_press;
  /** @brief Global anti-tremor time for release */
  uint16_t debounce_release;
  /** @brief Global anti-tremor time for idle */
  uint16_t debounce_idle;
  /** @brief Enable/disable button learning mode
   * @todo Move to "volatile" storage, independent from slot change */
  uint8_t button_learn;
  /** @brief Feedback mode.
   * 
   * * 0 disables LED and buzzer
   * * 1 disables buzzer, but LED is on
   * * 2 disables LED, but buzzer gives output
   * * 3 gives LED and buzzer feedback
   * */
  uint8_t feedback;
  /** @brief Anti-tremor (debounce) time for press of each VB */
  uint16_t debounce_press_vb[32];
  /** @brief Anti-tremor (debounce) time for release of each VB */
  uint16_t debounce_release_vb[8*4];
  /** @brief Anti-tremor (debounce) time for idle of each VB */
  uint16_t debounce_idle_vb[8*4];
  /** @brief Slotname of this config */
  char slotName[50];
} generalConfig_t;

/** @brief One VB command (not HID), maybe an element of a command chain */
typedef struct vb_cmd vb_cmd_t;

/** @brief One VB command (not HID), maybe an element of a command chain 
 * 
 * This struct is used for VB commands, which are not HID commands.
 * 
 * @todo Document this struct more.
 * @see task_vb_addCmd
 * @see task_vb_clearCmds
 * @see task_vb_getCmdChain
 * @see task_vb_setCmdChain */
struct vb_cmd {
  /** @brief Number of virtual button. MSB signals if this is a press or
   * release action:
   * * If it is set (mask: 0x80), it is a press action
   * * If it is cleared, it is a release action 
   * */
  uint8_t vb;
  /** @brief Type of command */
  vb_cmd_type_t cmd;
  /** @brief Original AT command string, might be NULL if not used */
  char *atoriginal;
  /** @brief Parameter string, e.g. for slot names. Might be NULL if not necessary */
  char *cmdparam;
  /** @brief Pointer to next VB command element, might be NULL. */
  struct vb_cmd *next;
};


/** @brief One HID command, maybe an element of a command chain */
typedef struct hid_cmd hid_cmd_t;

/** @brief  One HID command, maybe an element of a command chain
 *
 * This struct is used either as one element to be passed to hal_serial
 * or the BLE class OR it is used to form a chained list of all HID
 * commands, which are currently active. task_hid takes care
 * of maintaining a list of all active VBs, if one gets triggered (via
 * task_debouncer), the HID task walks through this list for one or more
 * (in case of multiple press/release actions) HID commands, which are
 * sent to the BLE/USB HAL.
 * @see task_hid_addCmd
 * @see task_hid_clearCmds
 * @see task_hid_getCmdChain
 * @see task_hid_setCmdChain */
struct hid_cmd {
  /** @brief Number of virtual button. MSB signals if this is a press or
   * release action:
   * * If it is set (mask: 0x80), it is a press action
   * * If it is cleared, it is a release action 
   * */
  uint8_t vb;
  /** @brief Command to be sent, see HID_kbdmousejoystick.cpp or the
   * usb_bridge for explanations. */
  uint8_t cmd[3];
  /** @brief Original AT command string, might be NULL if not used */
  char *atoriginal;
  /** @brief Pointer to next HID command element, might be NULL. */
  struct hid_cmd *next;
};

/** @brief State of IR receiver
 * 
 * Following states are possible for the IR receiver (task):
 * * IR_IDLE Nothing is done, this halIOIR_t struct is not used for receiving
 * * IR_RECEIVING Receiver is active and storing
 * * IR_TOOSHORT If timeout was triggered and not enough edges are detected
 * * IR_FINISHED If timeout was triggered and enough edges were stored
 * * IR_OVERFLOW Too many edges were detected, could not store
 * 
 * @see TASK_HAL_IR_RECEV_MINIMUM_EDGES
 * @see TASK_HAL_IR_RECV_MAXIMUM_EDGES*/
typedef enum irstate {IR_IDLE,IR_RECEIVING,IR_TOOSHORT,IR_FINISHED,IR_OVERFLOW} irstate_t;

/** @brief Output IR command */
typedef struct halIOIR {
  /** Buffer for IR signal
   * @warning Do not free this buffer! It will be freed by transmitting function
   * @note In case of receiving, this buffer can be freed. */
  rmt_item32_t *buffer;
  /** Count of rmt_item32_t items */
  uint16_t count;
  /** Status of receiver */
  irstate_t status;
} halIOIR_t;

/** @brief Strips away \\r\\t and \\n */
void strip(char *s);

/** @brief NVS key for wifi password */
#define NVS_WIFIPW  "nvswifipw"

/** @brief Minutes between last client disconnected and WiFi is switched off */
#define WIFI_OFF_TIME 5


#endif /*FUNCTION_COMMON_H_*/
