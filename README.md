 # Line based command parser
 
 This tool is used to parse fixed commands and execute the corresponding command handlers. To have an easy, short and maintainable parser,
 we don't allocate the commands dynamically (like most parsers with some kind of _addCommand_ ), we have a constant array of
 structs for each command.
 
 Therefore, the full command table is resided in flash memory, with minimal stack usage. The parser itself is done in ~160 lines (including comments).
 
 ## Structure
 
 * The function _cmdParser_ is called with a full line string (line termination handling has to be done before calling the parser)
 * The array of _cmd_ structs is searched for a valid command. If found, the corresponding handler is called with the given parameter structure
 
 ## Configuration
 
 A few configuration is needed to setup the parser:
 
 * CMD_PREFIX - this will be the prefix, which is always put in front of a command. In the example case "AT ". For some modems you could use "AT+".
 * CMD_LENGTH - fixed length of the command, directly following the prefix. "AT+LA" would result in a command length of 2.
 * CMD_MAXLENGTH - maximum length of a full line. Buffer checking is limited to this value, so make sure the allocated buffer is at least this size.
 * The 'cmd' struct needs to be filled with your commands.
 
 ## Remarks
 
 * Due to the limits within the command structure, uint32_t won't have full value range. Upper limit is the same as for int32_t

 
 ## Porting
 
 Although this code is written as an ESP32 application, the parser itself needs minor modification to be used on another platform:
 
 * Remove ESP32 related logging ( ESP_LOG* )
 * Check how to place the command struct into flash memory (e.g., for AVR systems you need the _PROGMEM_ directive and the
 corresponding _pgm_read_byte_
 * The parser itself works with a char string, so no adaptions needed there.
 
 ## Issues
 
 If you find any issues or enhancements, please report here or open a pull request
 
